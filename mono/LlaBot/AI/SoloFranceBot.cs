﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class SoloFranceBot : Bot
    {
        List<double> throttle = new List<double>();
        CarPosition previousPosition;

        public SoloFranceBot(Track track)
        {
            if (track == null)
                throw new ArgumentNullException("track");
            Track = track;
            initThrottle();
        }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            previousPosition = Position;
            Position = carPositions[0];
            return new Command(getThrottle(), getSwitchLane(), getTurbo());
        }

        double getThrottle()
        {
            if (Position.IsSame(previousPosition))
                return 1.0;
            if (Lap == 0 && Position.PieceIndex == 0)
                return 1.0;
            if (isInBreakingArea())
                return 0.0;
            if (Lap == 1 && Position.PieceIndex >= 38)
                return 0.7;
            return 1.0;
        }

        bool isInBreakingArea()
        {
            int first = Lap == 2 ? 16 : 0;
            IList<double> curves = Track.GetCurveStarts();
            for (int i = 0; i < curves.Count; i++)
            {
                if (CarDistance > curves[i] + throttle[first + 2 * i] &&
                    CarDistance < curves[i] + throttle[first + 2 * i + 1])
                    return true;
            }
            return false;
        }

        SwitchLane getSwitchLane()
        {
            if (Lap == 0 && Position.PieceIndex == 1)
                return SwitchLane.Right;
            switch (Position.PieceIndex)
            {
                case 6:
                    return SwitchLane.Left;
                case 16:
                    return SwitchLane.Right;
                default:
                    return SwitchLane.None;
            }
        }

        bool getTurbo()
        {
            if (!IsTurboAvailable)
                return false;
            bool result = false;
            if (Position.Lap == 1)
                result = Position.PieceIndex == 38 && Position.InPieceDistance >= 0.0;
            else if (Position.Lap == 2)
                result = Position.PieceIndex == 37 && Position.InPieceDistance >= 0.0;
            if (result)
                UseTurbo();
            return result;
        }

        void initThrottle()
        {
            throttle.Add(-200.0); //  0
            throttle.Add(210.0);    //  1
            throttle.Add(-60.0); //  2
            throttle.Add(0.0);    //  3
            throttle.Add(-100.0); //  4
            throttle.Add(0.0);   //  5
            throttle.Add(-150.0);  //  6
            throttle.Add(0.0);    //  7
            throttle.Add(-220.0); //  8
            throttle.Add(0.0);  //  9
            throttle.Add(-20.0); // 10
            throttle.Add(60.0);   // 11
            throttle.Add(-40.0); // 12
            throttle.Add(0.0);   // 13
            throttle.Add(-80.0);    // 14
            throttle.Add(0.0);   // 15
            int count = throttle.Count;
            for (int i = 0; i < count; i++)
                throttle.Add(throttle[i]);
        }
    }
}
