﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class SoloGermanyBot : Bot
    {
        CarPosition previousPosition;
        List<double> throttle = new List<double>(52);

        public SoloGermanyBot(Track track)
        {
            if (track == null)
                throw new ArgumentNullException("track");

            Track = track;
            initThrottle();
        }


        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            Position = carPositions[0];
            Command command = new Command(getThrottle(), getSwitchLane(), getTurbo());
            previousPosition = Position;
            return command;
        }

        double getThrottle()
        {
            if (Position.IsSame(previousPosition))
                return 1.0;
            if (Lap == 0 && Position.PieceIndex == 0)
                return 1.0;
            if (isInBreakingArea())
                return 0.0;
            if (Lap == 1 && CarDistance > getLap1TurboStart())
            {
                return 0.4;
            }
            return 1.0;
        }

        bool isInBreakingArea()
        {
            int first = Lap == 2 ? 26 : 0;
            IList<double> curves = Track.GetCurveStarts();
            for (int i = 0; i < curves.Count; i++)
            {
                if (CarDistance > curves[i] + throttle[first + 2 * i] &&
                    CarDistance < curves[i] + throttle[first + 2 * i + 1])
                    return true;
            }
            if (CarDistance >
                Track.GetLength() + throttle[0] + Track.Pieces[0].Length + Track.Pieces[1].Length)
                return true;
            return false;
        }

        SwitchLane getSwitchLane()
        {
            if (Position.PieceIndex == getPreviousPieceIndex())
                return SwitchLane.None;

            switch(Position.PieceIndex)
            {
                case 0:
                    return Lap == 0 ? SwitchLane.Left : SwitchLane.None;
                case 26:
                    return SwitchLane.Left;
                case 13:
                case 35:
                    return SwitchLane.Right;
                case 52:
                    return Lap < 2 ? SwitchLane.Left : SwitchLane.None;
                default:
                    return SwitchLane.None;
            }
        }

        bool getTurbo()
        {
            if (!IsTurboAvailable)
                return false;

            switch (Lap)
            {
                case 1:
                    return CarDistance > getLap1TurboStart();
                case 2:
                    return CarDistance > getLap2TurboStart();
                default:
                    return false;
            }
        }

        double getLap1TurboStart()
        {
            //return Track.GetPieceStartPosition(54);
            return Double.MaxValue;
        }

        double getLap2TurboStart()
        {
            return Track.GetPieceStartPosition(54);
        }

        void initThrottle()
        {
            /**
            throttle.Add(-400.0); //  0
            throttle.Add(50.0);    //  1
            throttle.Add(-100.0); //  2
            throttle.Add(50.0);  //  3
            throttle.Add(-100.0); //  4
            throttle.Add(50.0);  //  5
            throttle.Add(-50.0);  //  6
            throttle.Add(0.0);    //  7
            throttle.Add(-100.0);    //  8
            throttle.Add(100.0);  //  9
            throttle.Add(-100.0); // 10
            throttle.Add(50.0);    // 11
            throttle.Add(-100.0); // 12
            throttle.Add(50.0);   // 13
            throttle.Add(0.0);    // 14
            throttle.Add(50.0);   // 15
            throttle.Add(-100.0); // 16
            throttle.Add(0.0);    // 17
            throttle.Add(-50.0);  // 18
            throttle.Add(5.0);    // 19
            throttle.Add(-125.0); // 20
            throttle.Add(0.0);    // 21
            throttle.Add(-200.0); // 22
            throttle.Add(0.0);    // 23
            throttle.Add(-200.0); // 24
            throttle.Add(0.0);    // 25
            /**/
            List<double> genes = throttle;
            genes.Add(-200.0); //  0
            genes.Add(50.0);    //  1
            genes.Add(-100.0); //  2
            genes.Add(50.0);  //  3
            genes.Add(-100.0); //  4
            genes.Add(50.0);  //  5
            genes.Add(-50.0);  //  6
            genes.Add(0.0);    //  7
            genes.Add(-50.0);  //  8
            genes.Add(0.0);    //  9
            genes.Add(-100.0);    // 10
            genes.Add(100.0);  //  11
            genes.Add(-100.0); // 12
            genes.Add(50.0);    // 13
            genes.Add(-100.0); // 14
            genes.Add(50.0);   // 15
            genes.Add(0.0);    // 16
            genes.Add(50.0);   // 17
            genes.Add(-100.0); // 18
            genes.Add(0.0);    // 19
            genes.Add(-100.0);  // 20
            genes.Add(5.0);    // 21
            genes.Add(-125.0); // 22
            genes.Add(0.0);    // 23
            genes.Add(-200.0); // 24
            genes.Add(0.0);    // 25
            genes.Add(-200.0); // 26
            genes.Add(0.0);    // 27
            genes.Add(-200.0); // 28
            genes.Add(0.0);    // 29
            int count = throttle.Count;
            for (int i = 0; i < count; i++)
                throttle.Add(throttle[i]);
        }

        int getPreviousPieceIndex()
        {
            return previousPosition != null ? previousPosition.PieceIndex : 56;
        }
    }
}
