﻿using Lla.HwoBot.Race;

namespace Lla.HwoBot.AI
{
    class TestBotFactory : IBotFactory
    {
        public Bot CreateBot(Track track)
        {
            return new TestBot();
        }
    }
}
