﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class SoloFinlandBot : Bot
    {
        Track track;

        public SoloFinlandBot(Track track)
        {
            if (track == null)
                throw new ArgumentNullException("track");
            this.track = track;
        }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            CarPosition position = carPositions[0];
            return new Command(getThrottle(position), getSwitchLane(position), getTurbo(position));
        }

        double getThrottle(CarPosition position)
        {
            double start4 = track.GetPieceStartPosition(4);
            double start14 = track.GetPieceStartPosition(14);
            double start19 = track.GetPieceStartPosition(19);
            double start26 = track.GetPieceStartPosition(26);
            double start29 = track.GetPieceStartPosition(26);
            double start31 = track.GetPieceStartPosition(31);
            double car = track.GetPieceStartPosition(position.PieceIndex) +
                position.InPieceDistance;
            if (position.Lap == 2 && position.PieceIndex < 4)
                return 0.0;
            if (position.Lap == 1 && position.PieceIndex > 37 && position.InPieceDistance > 40.0)
                return 0.0;
            if (car > start4 - 120.0 && car < start4 + 100.0)
                return 0.0;
            else if (car > start14 - 100.0 && car < start14 + 100.0)
                return 0.0;
            else if (car > start19 - 50.0 && car < start19 + 90.0)
                return 0.0;
            else if (car > start29 - 100.0 && car < start29 + 30.0)
                return 0.0;
            else if (car > start31 - 90.0 && car < start31 + 65.0)
                return 0.0;
            else
                return 1.0;
        }

        SwitchLane getSwitchLane(CarPosition position)
        {
            if (position.PieceIndex == 2 && position.Lane == 0)
                return SwitchLane.Right;
            else if (position.PieceIndex == 7)
                return SwitchLane.Left;
            else if (position.PieceIndex == 17)
                return SwitchLane.Right;
            else
                return SwitchLane.None;
        }

        bool getTurbo(CarPosition position)
        {
            if (!IsTurboAvailable)
                return false;
            bool result = false;
            if (position.Lap == 1)
                result = position.PieceIndex == 34 && position.InPieceDistance >= 35.0;
            else if (position.Lap == 2)
                result = position.PieceIndex == 34 && position.InPieceDistance >= 35.0;
            if (result)
                UseTurbo();
            return result;
        }
    }
}
