﻿using Lla.HwoBot.Race;

namespace Lla.HwoBot.AI
{
    class ConstantSpeedBotFactory : IBotFactory
    {
        private readonly double speed;

        public ConstantSpeedBotFactory(double speed)
        {
            this.speed = speed;
        }

        public Bot CreateBot(Track track)
        {
            return new ConstantSpeedBot(speed);
        }
    }
}
