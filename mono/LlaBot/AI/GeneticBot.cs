﻿using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    abstract class GeneticBot : Bot
    {

        public GeneticBot(IList<double> genes, Random rng)
        {
            if (genes == null)
                throw new ArgumentNullException("genes");
            Genes = genes;
            Rng = rng;
        }

        public double Score { get; set; }

        protected IList<double> Genes { get; private set; }

        protected Random Rng { get; private set; }

        public abstract GeneticBot CreateOffspring();

        protected IList<double> CreateOffspringGenes()
        {
            List<double> genes = new List<double>(Genes.Count);
            foreach (double gene in Genes)
                genes.Add(mutate(gene));
            return genes;
        }

        public override string ToString()
        {
            return "Genes\t" + String.Join("\t", Genes) + "\tScore\t" + Score;
        }

        double mutate(double gene)
        {
            return gene + 5.0 * (Rng.NextDouble() * 2.0 - 1.0);
        }
    }
}
