﻿using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class GeneticSoloTrackSpecificBotFactory : IGeneticBotFactory
    {
        string trackName = "";
        readonly List<double> genes = new List<double>();
        readonly Random rng = new Random();

        public GeneticSoloTrackSpecificBotFactory(string trackName)
        {
            this.trackName = trackName;
            switch (trackName)
            {
                case "finland":
                    initGenesForFinland();
                    break;
                case "germany":
                    initGenesForGermany();
                    break;
                case "usa":
                    initGenesForUsa();
                    break;
                default:
                    initGenesForFinland();
                    break;
            }
        }

        public GeneticBot CreateBot()
        {
            switch (trackName)
            {
                case "finland":
                    return new GeneticSoloFinlandBot(genes, rng);
                case "germany":
                    return new GeneticSoloGermanyBot(genes, rng).CreateOffspring();
                case "usa":
                    return new GeneticSoloUsaBot(genes, rng);
                default:
                    return new GeneticSoloFinlandBot(genes, rng);
            }
        }

        void initGenesForFinland()
        {
            /*
            genes.Add(-120.0); // piece 4
            genes.Add(100);
            genes.Add(-100.0); // piece 14
            genes.Add(100.0);
            genes.Add(-50.0);  // piece 19
            genes.Add(90.0);
            genes.Add(-100.0); // piece 29
            genes.Add(30.0);
            genes.Add(-90.0);  // piece 31
            genes.Add(65);
            genes.Add(40.0);   // piece 38
            genes.Add(35.0);   // turbo 1
            genes.Add(35.0);   // turbo 2
            /**/
            /*
            genes.Add(-164.079466645643);  // piece 4
            genes.Add(122.622503481164);
            genes.Add(-59.3131819410777);  // piece 14
            genes.Add(128.765573687277);
            genes.Add(-18.1875817376131);  // piece 19
            genes.Add(112.492650790369);
            genes.Add(-25.2815501975276);  // piece 29
            genes.Add(128.880794317872);
            genes.Add(15.8202915339826);   // piece 31
            genes.Add(151.662888702314);
            genes.Add(241.381893200512);   // piece 38
            genes.Add(35.0);               // turbo 1
            genes.Add(30.0);               // turbo 2
            /**/
            genes.Add(-161.084269160033); // piece 4
            genes.Add(136.732592197816);
            genes.Add(-69.9182411439826); // piece 14
            genes.Add(126.076140910969);
            genes.Add(-16.3347339988705);  // piece 19
            genes.Add(119.935648395522);
            genes.Add(-22.4256756010838); // piece 29
            genes.Add(140.263674409687);
            genes.Add(9.47681035061632);  // piece 31
            genes.Add(158.47599144227);
            genes.Add(223.803856361071);   // piece 38
            genes.Add(36.4253988304545);   // turbo 1
            genes.Add(27.3919335402489);   // turbo 2
        }

        void initGenesForGermany()
        {

            genes.Add(-300.0); //  0
            genes.Add(50.0);    //  1
            genes.Add(-100.0); //  2
            genes.Add(50.0);  //  3
            genes.Add(-100.0); //  4
            genes.Add(50.0);  //  5
            genes.Add(-50.0);  //  6
            genes.Add(0.0);    //  7
            genes.Add(-50.0);  //  8
            genes.Add(0.0);    //  9
            genes.Add(-100.0);    // 10
            genes.Add(100.0);  //  11
            genes.Add(-100.0); // 12
            genes.Add(50.0);    // 13
            genes.Add(-100.0); // 14
            genes.Add(50.0);   // 15
            genes.Add(0.0);    // 16
            genes.Add(50.0);   // 17
            genes.Add(-100.0); // 18
            genes.Add(0.0);    // 19
            genes.Add(-100.0);  // 20
            genes.Add(5.0);    // 21
            genes.Add(-125.0); // 22
            genes.Add(0.0);    // 23
            genes.Add(-200.0); // 24
            genes.Add(0.0);    // 25
            genes.Add(-200.0); // 26
            genes.Add(0.0);    // 27
            genes.Add(-200.0); // 28
            genes.Add(0.0);    // 29
            int count = genes.Count;
            for (int i = 0; i < count; i++)
                genes.Add(genes[i]);
            genes.Add(50.0);    // 52
            genes.Add(0.0);     // 53
            genes.Add(0.0);     // 54
        }

        void initGenesForUsa()
        {/*
            // Lap 1
            genes.Add(50.0);
            genes.Add(0.0);
            genes.Add(50.0);
            genes.Add(0.0);
            genes.Add(50.0);
            genes.Add(0.0);
            genes.Add(50.0);
            genes.Add(0.0);

            // Lap 2
            genes.Add(50.0);
            genes.Add(0.0);
            genes.Add(50.0);
            genes.Add(0.0);
            genes.Add(400.0);
            genes.Add(0.0);
            genes.Add(50.0);
            genes.Add(0.0);

            // Turbo
            genes.Add(0.0);
            genes.Add(0.0);
*/
            // Lap 1
            genes.Add(-54.8246409882906);
            genes.Add(34.2229696685555);
            genes.Add(1.14021641022532);
            genes.Add(46.2221338698744);
            genes.Add(-16.4475673350727);
            genes.Add(109.140338582983);
            genes.Add(58.0269882609263);
            genes.Add(4.61597439815098);

            // Lap 2
            genes.Add(-15.1704539778505);
            genes.Add(57.7493072313952);
            genes.Add(-29.5139154440323);
            genes.Add(68.3281645473689);
            genes.Add(340.475800701173);
            genes.Add(85.961533943639);
            genes.Add(-28.157668450455);
            genes.Add(87.0326490383747);

            // Turbo
            genes.Add(-87.8522771749889);
            genes.Add(-118.145840006482);
/*
            // Lap 1
            genes.Add(6.91205327255283);
            genes.Add(-29.907384677747);
            genes.Add(66.3750876236591);
            genes.Add(-17.5117568799815);
            genes.Add(-9.75324656337185);
            genes.Add(75.9173044915858);
            genes.Add(71.9475236357877);
            genes.Add(-41.3963569520956);

            // Lap 2
            genes.Add(42.6511221763916);
            genes.Add(2.85393632149971);
            genes.Add(42.481800775268);
            genes.Add(-3.5093520178969);
            genes.Add(333.94720837192);
            genes.Add(30.3693292757353);
            genes.Add(0.766545571743785);
            genes.Add(85.1498845336725);

            // Turbo
            genes.Add(-5.89035836788377);
            genes.Add(-128.661891640472);
 */
            // Lap 1
            genes.Add(-61.2140322994506);
            genes.Add(11.6819527753079);
            genes.Add(-20.2508446016586);
            genes.Add(67.5220498710508);
            genes.Add(-42.9889498292417);
            genes.Add(125.616141779169);
            genes.Add(83.3156939890776);
            genes.Add(1.27579614113817);

            // Lap 2
            genes.Add(-24.9334774282452);
            genes.Add(96.578780336575);
            genes.Add(-13.1865797113565);
            genes.Add(96.7167698297262);
            genes.Add(365.28483234592);
            genes.Add(83.4563007268386);
            genes.Add(-5.26422683860369);
            genes.Add(78.8783263782404);

            // Turbo
            genes.Add(-102.456771681298);
            genes.Add(-134.353925005697);
        }
    }
}
