﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class SoloUsaBot : Bot
    {
        bool saveTurbo;

        public SoloUsaBot(Track track)
        {
            if (track == null)
                throw new ArgumentNullException("track");
            Track = track;
        }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            Position = carPositions[0];
            return new Command(getThrottle(), getSwitchLane(), getTurbo());
        }

        double getThrottle()
        {
            double piece2 = Track.GetPieceStartPosition(2);
            double piece8 = Track.GetPieceStartPosition(8);
            double piece18 = Track.GetPieceStartPosition(18);
            double piece24 = Track.GetPieceStartPosition(24);
            if (Position.Lap == 0)
                return 0.45;
            if (Position.Lap == 1)
            {
                if (CarDistance > piece2 - 50.0 && CarDistance < piece2 + 0.0)
                    return 0.0;
                if (CarDistance > piece8 - 50.0 && CarDistance < piece8 + 0.0)
                    return 0.0;
                if (CarDistance > piece18 - 50.0 && CarDistance < piece18 + 0.0)
                    return 0.0;
                if (CarDistance > piece24 - 50.0 && CarDistance < piece24 + 0.0)
                    return 0.0;
            }
            else if (Position.Lap == 2)
            {
                if (CarDistance > piece2 - 50.0 && CarDistance < piece2 + 0.0)
                    return 0.0;
                if (CarDistance > piece8 - 50.0 && CarDistance < piece8 + 0.0)
                    return 0.0;
                if (CarDistance > piece18 - 400.0 && CarDistance < piece18 + 0.0)
                    return 0.0;
                if (CarDistance > piece24 - 50.0 && CarDistance < piece24 + 0.0)
                    return 0.0;
            }
            return 1.0;
        }

        SwitchLane getSwitchLane()
        {
            if (Position.Lap == 0 && Position.PieceIndex == 0)
                return SwitchLane.Right;
            if (Position.Lap == 0 && Position.PieceIndex == 6)
                return SwitchLane.Right;
            return SwitchLane.None;
        }

        bool getTurbo()
        {
            if (!IsTurboAvailable || Position.Lap < 2)
                return false;

            if (CarDistance > Track.GetPieceStartPosition(12) && !saveTurbo)
            {
                saveTurbo = true;
                return true;
            }
            if (CarDistance > Track.GetPieceStartPosition(28))
                return true;
            return false;
        }
    }
}
