﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class ConstantSpeedBot : Bot
    {
        private readonly double speed;
        private bool innerLane;
        private double speed1;

        public ConstantSpeedBot(double speed)
        {
            if (speed <= 0.0)
                throw new ArgumentException("speed must be positive", "speed");
            this.speed = speed;
        }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            Command command = innerLane ? 
                new Command(speed, SwitchLane.None, /* turbo = */ false) : 
                new Command(speed, SwitchLane.Right, /* turbo = */ false);
            innerLane = true;
            return command;
        }
    }
}
