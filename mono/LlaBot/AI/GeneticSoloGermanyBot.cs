﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class GeneticSoloGermanyBot : GeneticBot
    {
        const int firstTurboThrottleGene = 60;
        const int firstTurboGene = 61;
        const int secondTurboGene = 62;
        bool saveTurbo = false;

        CarPosition previousPosition;

        public GeneticSoloGermanyBot(IList<double> genes, Random rng) : base(genes, rng) { }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            Position = carPositions[0];
            Command command = new Command(getThrottle(), getSwitchLane(), getTurbo());
            previousPosition = Position;
            return command;
        }

        public override GeneticBot CreateOffspring()
        {
            GeneticSoloGermanyBot offspring = new GeneticSoloGermanyBot(CreateOffspringGenes(), Rng);
            offspring.Track = Track;
            return offspring;
        }

        double getThrottle()
        {
            if (Position.IsSame(previousPosition))
                return 1.0;
            if (Lap == 0 && Position.PieceIndex == 0)
                return 1.0;
            if (isInBreakingArea())
                return 0.0;
            if (Lap == 1 && CarDistance > getLap1TurboStart())
                return Genes[firstTurboThrottleGene] / 100;
            return 1.0;
        }

        bool isInBreakingArea()
        {
            int first = Lap == 2 ? 30 : 0;
            IList<double> curves = Track.GetCurveStarts();
            for (int i = 0; i < curves.Count; i++)
            {
                if (CarDistance > curves[i] + Genes[first + 2 * i] &&
                    CarDistance < curves[i] + Genes[first + 2 * i + 1])
                    return true;
            }
            if (Lap < 2 && CarDistance >
                Track.GetLength() + Genes[0] + Track.Pieces[0].Length + Track.Pieces[1].Length)
                return true;
            return false;
        }

        SwitchLane getSwitchLane()
        {
            if (Position.PieceIndex == getPreviousPieceIndex())
                return SwitchLane.None;

            switch(Position.PieceIndex)
            {
                case 0:
                    return Lap == 0 ? SwitchLane.Left : SwitchLane.None;
                case 26:
                    return SwitchLane.Left;
                case 13:
                case 35:
                    return SwitchLane.Right;
                case 52:
                    return Lap < 2 ? SwitchLane.Left : SwitchLane.None;
                default:
                    return SwitchLane.None;
            }
        }

        bool getTurbo()
        {
            if (Lap > 0 && Lap > previousPosition.Lap)
                saveTurbo = false;
            if (!IsTurboAvailable || saveTurbo)
                return false;

            bool result = false;
            switch (Lap)
            {
                case 1:
                    result = CarDistance > getLap1TurboStart() + Genes[firstTurboGene];
                    break;
                case 2:
                    result = CarDistance > getLap2TurboStart() + Genes[secondTurboGene];
                    break;
            }
            if (result)
                saveTurbo = true;
            return result;
        }

        double getLap1TurboStart()
        {
            return Track.GetPieceStartPosition(54);
        }

        double getLap2TurboStart()
        {
            return Track.GetPieceStartPosition(54);
        }

        int getPreviousPieceIndex()
        {
            return previousPosition != null ? previousPosition.PieceIndex : 56;
        }
    }
}
