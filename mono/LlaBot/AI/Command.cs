﻿using System;

namespace Lla.HwoBot.AI
{
    class Command
    {
        public Command(double throttle, SwitchLane switchLane, bool turbo)
        {
            if (throttle < 0.0 || throttle > 1.0)
                throw new ArgumentException("Throttle must be between 0.0 and 1.0", "throttle");
            Throttle = throttle;
            SwitchLane = switchLane;
            Turbo = turbo;
        }

        public double Throttle { get; private set; }

        public SwitchLane SwitchLane { get; private set; }

        public bool Turbo { get; private set; }
    }
}
