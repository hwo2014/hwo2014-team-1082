﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class GeneticSoloFinlandBot : GeneticBot
    {
        public GeneticSoloFinlandBot(IList<double> genes, Random rng) : base(genes, rng) { }

        public override GeneticBot CreateOffspring()
        {
            GeneticSoloFinlandBot offspring = new GeneticSoloFinlandBot(CreateOffspringGenes(), Rng);
            offspring.Track = Track;
            return new GeneticSoloFinlandBot(CreateOffspringGenes(), Rng);
        }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            CarPosition position = carPositions[0];
            return new Command(getThrottle(position), getSwitchLane(position), getTurbo(position));
        }

        private double getThrottle(CarPosition position)
        {
            double start4 = Track.GetPieceStartPosition(4);
            double start14 = Track.GetPieceStartPosition(14);
            double start19 = Track.GetPieceStartPosition(19);
            double start26 = Track.GetPieceStartPosition(26);
            double start29 = Track.GetPieceStartPosition(26);
            double start31 = Track.GetPieceStartPosition(31);
            double start37 = Track.GetPieceStartPosition(37);
            double car = Track.GetPieceStartPosition(position.PieceIndex) +
                position.InPieceDistance;

            if (position.Lap == 2 && position.PieceIndex < 4 && (position.PieceIndex != 1 || position.InPieceDistance > 5.0))
                return 0.0;
            if (position.Lap == 1 && car > start37 + Genes[10])
                return 0.0;
            if (car > start4 + Genes[0] && car < start4 + Genes[1])
                return 0.0;
            else if (car > start14 + Genes[2] && car < start14 + Genes[3])
                return 0.0;
            else if (car > start19 + Genes[4] && car < start19 + Genes[5])
                return 0.0;
            else if (car > start29 + Genes[6] && car < start29 + Genes[7])
                return 0.0;
            else if (car > start31 + Genes[8] && car < start31 + Genes[9])
                return 0.0;
            else
                return 1.0;
        }

        private SwitchLane getSwitchLane(CarPosition position)
        {
            if (position.PieceIndex == 2 && position.Lane == 0)
                return SwitchLane.Right;
            else if (position.PieceIndex == 7)
                return SwitchLane.Left;
            else if (position.PieceIndex == 17)
                return SwitchLane.Right;
            else
                return SwitchLane.None;
        }

        private bool getTurbo(CarPosition position)
        {
            if (!IsTurboAvailable)
                return false;
            bool result = false;
            if (position.Lap == 1)
                result = position.PieceIndex == 34 && position.InPieceDistance >= Genes[11];
            else if (position.Lap == 2)
                result = position.PieceIndex == 34 && position.InPieceDistance >= Genes[12];
            if (result)
                UseTurbo();
            return result;
        }
    }
}
