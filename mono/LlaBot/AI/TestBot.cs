﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class TestBot : Bot
    {
        bool innerLane;

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            Position = carPositions[0];
            double throttle = 1.0;
            if (Position.PieceIndex > 4 || Position.PieceIndex == 4 && Position.InPieceDistance > 36)
                throttle = 0.801355;
            if (Position.PieceIndex > 10)
                throttle = 1.0;
            Command command = innerLane ?
                new Command(throttle, SwitchLane.None, /* turbo = */ false) :
                new Command(throttle, SwitchLane.Right, /* turbo = */ false);
            innerLane = true;
            return command;
        }
    }
}
