﻿using Lla.HwoBot.Race;
using System;

namespace Lla.HwoBot.AI
{
    class SoloTrackSpecificBotFactory : IBotFactory
    {
        string trackName;

        public SoloTrackSpecificBotFactory(string trackName)
        {
            this.trackName = trackName;
        }

        public Bot CreateBot(Track track)
        {
            switch (trackName)
            {
                case "finland":
                    return new SoloFinlandBot(track);
                case "france":
                    return new SoloFranceBot(track);
                case "germany":
                    return new SoloGermanyBot(track);
                case "usa":
                    return new SoloUsaBot(track);
                default:
                    return new SoloFinlandBot(track);
            }
            
        }
    }
}
