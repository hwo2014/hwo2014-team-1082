﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    class GeneticSoloUsaBot : GeneticBot
    {
        bool saveTurbo;

        public GeneticSoloUsaBot(IList<double> genes, Random rng) : base(genes, rng) { }

        public override GeneticBot CreateOffspring()
        {
            GeneticSoloUsaBot offspring = new GeneticSoloUsaBot(CreateOffspringGenes(), Rng);
            offspring.Track = Track;
            return new GeneticSoloUsaBot(CreateOffspringGenes(), Rng);
        }

        public override Command GetCommand(IList<CarPosition> carPositions)
        {
            Position = carPositions[0];
            return new Command(getThrottle(), getSwitchLane(), getTurbo());
        }

        private double getThrottle()
        {
            double piece2 = Track.GetPieceStartPosition(2);
            double piece8 = Track.GetPieceStartPosition(8);
            double piece18 = Track.GetPieceStartPosition(18);
            double piece24 = Track.GetPieceStartPosition(24);
            if (Position.Lap == 0)
                return 0.45;
            else if (Position.Lap == 1)
            {
                if (CarDistance > piece2 - Genes[0] && CarDistance < piece2 + Genes[1])
                    return 0.0;
                if (CarDistance > piece8 - Genes[2] && CarDistance < piece8 + Genes[3])
                    return 0.0;
                if (CarDistance > piece18 - Genes[4] && CarDistance < piece18 + Genes[5])
                    return 0.0;
                if (CarDistance > piece24 - Genes[6] && CarDistance < piece24 + Genes[7])
                    return 0.0;
            }
            else if (Position.Lap == 2)
            {
                if (CarDistance > piece2 - Genes[8] && CarDistance < piece2 + Genes[9])
                    return 0.0;
                if (CarDistance > piece8 - Genes[10] && CarDistance < piece8 + Genes[11])
                    return 0.0;
                if (CarDistance > piece18 - Genes[12] && CarDistance < piece18 + Genes[13])
                    return 0.0;
                if (CarDistance > piece24 - Genes[14] && CarDistance < piece24 + Genes[15])
                    return 0.0;
            }
            return 1.0;
        }

        SwitchLane getSwitchLane()
        {
            if (Position.Lap == 0 && Position.PieceIndex == 0)
                return SwitchLane.Right;
            if (Position.Lap == 0 && Position.PieceIndex == 6)
                return SwitchLane.Right;
            return SwitchLane.None;
        }

        bool getTurbo()
        {
            if (!IsTurboAvailable || Position.Lap < 2)
                return false;

            if (CarDistance > Track.GetPieceStartPosition(12) + Genes[16] && !saveTurbo)
            {
                saveTurbo = true;
                return true;
            }
            if (CarDistance > Track.GetPieceStartPosition(28) + Genes[17])
                return true;
            return false;
        }
    }
}
