﻿using Lla.HwoBot.Race;

namespace Lla.HwoBot.AI
{
    interface IGeneticBotFactory
    {
        GeneticBot CreateBot();
    }
}
