﻿using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lla.HwoBot.AI
{
    class RaceBot
    {
        string myColor;
        List<Car> cars;
        List<List<CarPosition>> carPositionsHistory = new List<List<CarPosition>>();
        Track track;
        List<TrackPiece> pieces;
        bool isCrashed;
        Physics physics;
        bool speedInitialized;
		bool isTurboAvailable;

		public RaceBot(string myColor, List<Car> cars, Track track, Physics physics)
        {
            if (String.IsNullOrEmpty(myColor))
                throw new ArgumentException("myColor must not be empty", "myColor");
            if (track == null)
                throw new ArgumentNullException("cars");
			if (cars == null)
				throw new ArgumentNullException("track");
			if (physics == null)
				throw new ArgumentNullException("physics");
            this.myColor = myColor;
            this.cars = cars;
            this.track = track;
            this.pieces = track.Pieces;
			this.physics = physics;
        }

        CarPosition position
        {
            get
            {
                if (carPositionsHistory.Count == 0)
                    return null;
                List<CarPosition> positions = carPositionsHistory[carPositionsHistory.Count - 1];
                return positions.Find(p => p.Color == myColor);
            }
        }

        CarPosition previousPosition
        {
            get
            {
                if (carPositionsHistory.Count <= 1)
                    return null;
                List<CarPosition> positions = carPositionsHistory[carPositionsHistory.Count - 2];
                return positions.Find(p => p.Color == myColor);
            }
        }

        public void UpdatePositions(List<CarPosition> carPositions)
        {
            if (carPositions == null)
                throw new ArgumentNullException("carPositions");
            carPositionsHistory.Add(carPositions);
            if (speedInitialized)
                return;

            for (int i = 0; i < carPositionsHistory.Count - 3; i++)
            {
                CarPosition p0 = carPositionsHistory[i].Find(p => p.Color == myColor);
                CarPosition p1 = carPositionsHistory[i + 1].Find(p => p.Color == myColor);
                if (p1.InPieceDistance <= 0.000001)
                    continue;
                CarPosition p2 = carPositionsHistory[i + 2].Find(p => p.Color == myColor);
                CarPosition p3 = carPositionsHistory[i + 3].Find(p => p.Color == myColor);
                physics.InitSpeed(p0, p1, p2, p3);
                speedInitialized = true;
            }
        }

        public void Crash()
        {
            isCrashed = true;
			isTurboAvailable = false;
            physics.Crash(previousPosition, position);
        }

        public void Spawn()
        {
            isCrashed = false;
        }

		public void Turbo()
		{
			if (!isCrashed)
				isTurboAvailable = true;
		}

        public Command GetCommand()
        {
			Command cmd;
            if (isCrashed || previousPosition == null)
				cmd = new Command(1.0, SwitchLane.None, /* turbo = */ false);
			else
				cmd = new Command(getThrottle(), getSwitchLane(), getTurbo());
			Console.WriteLine (cmd);
			return cmd;
        }

        double getThrottle()
        {
			if (position.IsSame(previousPosition))
				return 1.0;

            double throttle = 1.0;
            TrackPiece piece = pieces[position.PieceIndex];
            if (piece.IsTurn)
            {
                double speed = physics.GetSpeed(previousPosition, position);
                double crashSpeed = physics.GetCrashSpeed(piece, position.Lane);
                if (crashSpeed < Double.PositiveInfinity)
                    throttle =  speed < crashSpeed ? physics.GetThrottleForTerminalSpeed(crashSpeed) : 0.0;
				//Console.WriteLine ("turn: " + speed + " " + crashSpeed + " " + throttle);
            }
            else
            {
				double speed = physics.GetSpeed(previousPosition, position);
                double distanceToTurn = physics.GetDistanceToNextTurn(position);
                double breakingDistance = physics.GetBreakingDistanceForNextTurn(previousPosition, position);
				throttle = distanceToTurn > breakingDistance + speed ? 1.0 : 0.0;
				//Console.WriteLine ("straight: " + distanceToTurn + " " + breakingDistance + " " + speed + " " + throttle);
            }
            return Math.Max(0.0, Math.Min(1.0, throttle));
        }

        SwitchLane getSwitchLane()
        {
			int nextIndex = (position.PieceIndex + 1) % pieces.Count;
			if (!pieces [nextIndex].HasSwitch)
				return SwitchLane.None;
			if (position.PieceIndex == previousPosition.PieceIndex)
				return SwitchLane.None;

			TrackPiece piece = track.GetNextTurnPieceIndex (position.PieceIndex);
			int targetLane = piece.Angle > 0 ? track.LaneCount : 0;

			if (position.Lane > targetLane)
				return SwitchLane.Left;
			if (position.Lane < targetLane)
				return SwitchLane.Right;
            return SwitchLane.None;
        }

        bool getTurbo()
        {
			if (!isTurboAvailable)
				return false;
			if (track.GetFirstPieceOfLongestStraight () != position.PieceIndex)
				return false;
			isTurboAvailable = false;
			return true;
        }
    }
}
