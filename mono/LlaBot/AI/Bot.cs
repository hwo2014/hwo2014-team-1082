﻿using Lla.HwoBot.Race;
using System.Collections.Generic;

namespace Lla.HwoBot.AI
{
    abstract class Bot
    {
        public Track Track { get; set; }

        protected bool IsTurboAvailable { get; private set; }

        protected CarPosition Position { get; set; }

        protected double CarDistance
        {
            get
            {
                return Track.GetPieceStartPosition(Position.PieceIndex) + Position.InPieceDistance;
            }
        }

        protected int Lap
        {
            get
            {
                return Position.Lap;
            }
        }

        public abstract Command GetCommand(IList<CarPosition> carPositions);

        public void ProvideTurbo()
        {
            IsTurboAvailable = true;
        }

        protected void UseTurbo()
        {
            IsTurboAvailable = false;
        }
    }
}
