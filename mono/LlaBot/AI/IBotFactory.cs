﻿using Lla.HwoBot.Race;

namespace Lla.HwoBot.AI
{
    interface IBotFactory
    {
        Bot CreateBot(Track track);
    }
}
