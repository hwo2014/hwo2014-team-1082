﻿using System;
namespace Lla.HwoBot.Racing
{
    class ConnectionParameters
    {
        string[] servers;

        public ConnectionParameters(string[] servers, string host, int port, string botName, 
            string botKey)
        {
            if (servers == null)
                throw new ArgumentNullException("servers");
            this.servers = servers;
            Host = host;
            Port = port;
            BotName = botName;
            BotKey = botKey;
        }

        public string Host { get; private set; }

        public int Port { get; private set; }

        public string BotName { get; private set; }

        public string BotKey { get; private set; }

        public int ServerCount
        {
            get
            {
                return servers.Length;
            }
        }

        public string GetServer(int index)
        {
            if (index < 0)
                return Host;
            else
                return servers[index] + "." + Host.Substring(Host.IndexOf('.') + 1);
        }
    }
}
