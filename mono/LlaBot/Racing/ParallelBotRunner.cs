﻿using Lla.HwoBot.AI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Lla.HwoBot.Racing
{
    class ParallelBotRunner
    {
        readonly ConnectionParameters connParams;
        readonly int botCount;

        public ParallelBotRunner(ConnectionParameters connParams, int botCount)
        {
            if (connParams == null)
                throw new ArgumentNullException("connParams");
            if (botCount < 1)
                throw new ArgumentException("botCount must be positive", "botCount");
            this.connParams = connParams;
            this.botCount = botCount;
        }

        public void Run(IBotFactory botFactory, string trackName)
        {
            if (botFactory == null)
                throw new ArgumentNullException("botFactory");

            List<Thread> threads = new List<Thread>(botCount);
            for (int i = 0; i < botCount; i++)
            {
                threads.Add(new Thread(p => {
                    int threadId = (int)p;
                    BotRunner runner = new BotRunner(connParams);
                    runner.Run(threadId, botFactory, trackName);
                    Console.WriteLine("Time: " + runner.BestLapMillis + " (" + threadId + ")");
                }));
                threads[i].Start(i);
            }
            threads.ForEach(t => t.Join());
        }
    }
}
