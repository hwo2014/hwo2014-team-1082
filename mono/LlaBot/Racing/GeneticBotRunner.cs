﻿using Lla.HwoBot.AI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Lla.HwoBot.Racing
{
    class GeneticBotRunner
    {
        const int threadCount = 40;
        readonly ConnectionParameters connParams;
        readonly int populationSize;
        readonly int selectionSize;
        List<GeneticBot> population;
        List<GeneticBot> initialPopulation = new List<GeneticBot>();
        string trackName;
        readonly ServerManager servers;

        public GeneticBotRunner(ConnectionParameters connParams, int populationSize)
        {
            if (connParams == null)
                throw new ArgumentNullException("connParams");
            if (populationSize < 1)
                throw new ArgumentException("populationSize must be positive", "populationSize");
            this.connParams = connParams;
            this.populationSize = populationSize;
            servers = new ServerManager(connParams.ServerCount);
            selectionSize = populationSize / 5;
        }

        public void Run(IGeneticBotFactory botFactory, string trackName)
        {
            if (botFactory == null)
                throw new ArgumentNullException("botFactory");

            this.trackName = trackName;
            population = CreateFirstGeneration(botFactory);
            initialPopulation.AddRange(population);
            for (int generation = 1; ; generation++)
            {
                Console.WriteLine("Generation " + generation);
                runPopulation(generation);
                population.Sort(delegate(GeneticBot bot1, GeneticBot bot2)
                {
                    if (bot1.Score == bot2.Score)
                        return 0;
                    if (bot1.Score < 0)
                        return 1;
                    if (bot2.Score < 0)
                        return -1;
                    return (int) Math.Round(bot1.Score - bot2.Score);
                });
                population.ForEach(bot =>
                {
                    if (bot.Score > 0.0)
                        Console.WriteLine(bot.Score);
                });
                logResults(generation);
                naturalSelection();
            }
        }

        List<GeneticBot> CreateFirstGeneration(IGeneticBotFactory botFactory)
        {
            List<GeneticBot> bots = new List<GeneticBot>(populationSize);
            for (int i = 0; i < populationSize; i++)
                bots.Add(botFactory.CreateBot());
            return bots;
        }

        void runPopulation(int generation)
        {
            DateTime runStartTime = DateTime.Now;
            List<Thread> threads = new List<Thread>(threadCount);
            Queue<GeneticBot> botQueue = new Queue<GeneticBot>(population);
            for (int i = 0; i < threadCount; i++)
            {
                threads.Add(new Thread(threadId =>
                {
                    //for (int j = 0; j < populationSize / threadCount; j++)
                    int botIndex;
                    while (true)
                    {
                        GeneticBot bot;
                        Console.WriteLine("BeforeLock: " + threadId + " " + DateTime.Now);
                        lock (botQueue)
                        {
                            if (botQueue.Count > 0)
                                bot = botQueue.Dequeue();
                            else
                                break;
                            botIndex = populationSize - botQueue.Count;
                        }
                        Console.WriteLine("Wait for server: " + threadId + " " + DateTime.Now);
                        int serverIndex = servers.waitAvailable();
                        DateTime dateTime = DateTime.Now;
                        Console.WriteLine("Run bot: " + threadId + " " + DateTime.Now);
                        BotRunner runner = new BotRunner(connParams, serverIndex);
                        runner.Run(generation * 1000 + i, bot, trackName);
                        bot.Score = runner.BestLapMillis;
                        Console.WriteLine(botIndex + ": " + bot.Score + " " + (DateTime.Now.Ticks - dateTime.Ticks) / 10000 / 1000.0 + 
                            " (" + serverIndex + ", " + threadId + ")");
                    }
                    Console.WriteLine("Finished: " + threadId);
                }));
                threads[(int)i].Start((int)i);
            }

            threads.ForEach(t => t.Join());
            Console.WriteLine("Run time: " + (DateTime.Now - runStartTime).Ticks / 10000 / 1000.0);
        }

        void logResults(int generation)
        {
            StringBuilder log = new StringBuilder();
            population.ForEach(bot => log.AppendLine("Generation\t" + generation + "\t" + bot.ToString()));
            System.IO.File.AppendAllText("genetic.txt", log.ToString());
        }

        void naturalSelection()
        {
            int offsprings = population.Count / selectionSize;
            int firstCrasher = population.FindIndex(p => p.Score < 0);
            if (firstCrasher >= 0)
            {
                for (int i = 0; i < selectionSize - firstCrasher; i++)
                    population[firstCrasher + i] = initialPopulation[i];
            }
            for (int i = 0; i < selectionSize; i++)
            {
                for (int j = offsprings - 1; j >= 0; j--)
                    population[i + j * selectionSize] = population[i].CreateOffspring();
            }
        }
    }

    class ServerManager
    {
        List<Server> servers;

        public ServerManager(int serverCount)
        {
            if (serverCount <= 0)
                throw new ArgumentException("serverCount must be positive", "serverCount");
            servers = new List<Server>(serverCount);
            for (int i = 0; i < serverCount; i++)
                servers.Add(new Server());
        }

        public int waitAvailable()
        {
            while (true)
            {
                lock (servers)
                {
                    for (int i = 0; i < servers.Count; i++)
                    {
                        if (servers[i].hasAvailableSlot())
                        {
                            servers[i].useSlot();
                            return i;
                        }
                    }
                }
                Thread.Sleep(100);
            }
        }

        class Server
        {
            List<DateTime> connectionTimes = new List<DateTime>();

            public bool hasAvailableSlot()
            {
                connectionTimes.RemoveAll(p => p.AddSeconds(120) < DateTime.Now);
                return connectionTimes.Count < 10;
            }

            public void useSlot()
            {
                connectionTimes.Add(DateTime.Now);
            }
        }
    }
}
