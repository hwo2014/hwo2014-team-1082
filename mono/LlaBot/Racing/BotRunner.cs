﻿using Lla.HwoBot.AI;
using Lla.HwoBot.Messages;
using Lla.HwoBot.Messages.Assemblers;
using Lla.HwoBot.Messages.Dtos;
using Lla.HwoBot.Race;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Lla.HwoBot.Racing
{
    class BotRunner
    {
        readonly ConnectionParameters connParams;
        readonly int serverIndex;
        StreamReader reader;
        StreamWriter writer;
        StringBuilder fullLog = new StringBuilder();
        IBotFactory botFactory;
        Bot bot;
        bool logEnabled = true;
        int currentPieceIndex;
        double previousThrottle;
        string trackName;

        public BotRunner(ConnectionParameters connectionParams)
        {
            if (connectionParams == null)
                throw new ArgumentNullException("connectionParams");
            this.connParams = connectionParams;
            this.serverIndex = -1;
            BestLapMillis = -1;
        }

        public BotRunner(ConnectionParameters connectionParams, int serverIndex)
        {
            if (connectionParams == null)
                throw new ArgumentNullException("connectionParams");
            this.connParams = connectionParams;
            this.serverIndex = serverIndex;
            this.logEnabled = false;
            BestLapMillis = -1;
        }

        public int BestLapMillis { get; private set; }

        public void Run(int runId, IBotFactory botFactory, string trackName)
        {
            if (botFactory == null)
                throw new ArgumentNullException("botFactory");

            this.botFactory = botFactory;
            this.trackName = trackName;
            run(runId);
        }

        public void Run(int runId, Bot bot, string trackName)
        {
            if (bot == null)
                throw new ArgumentNullException("bot");
            this.bot = bot;
            this.trackName = trackName;
            run(runId);
        }

        void run(int runId)
        {
            log("Connecting to " + connParams.GetServer(serverIndex) + ":" + connParams.Port + " as " + 
                connParams.BotName + "/" + connParams.BotKey);

            try
            {
                using (TcpClient client = new TcpClient(connParams.GetServer(serverIndex), connParams.Port))
                {
                    NetworkStream stream = client.GetStream();
                    reader = new StreamReader(stream);
                    writer = new StreamWriter(stream);
                    writer.AutoFlush = true;
                    run();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                tryToSaveLog(runId);
            }
        }

        void tryToSaveLog(int runId)
        {
            string filename = "fulllog" + runId + ".txt";
            try
            {
                System.IO.File.WriteAllText(filename, "Best lap:" + BestLapMillis + "\n" + fullLog.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error writing file: " + filename + "\"" + ex.ToString() + "\"");
            }
        }

        void run()
        {
            if (trackName != "")
                send(new JoinTrackMessage(connParams.BotName, connParams.BotKey, trackName, 1));
            else
                send(new JoinMessage(connParams.BotName, connParams.BotKey));
            string line;
            currentPieceIndex = -1;
            previousThrottle = 0.0;
            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper<Object> msg = JsonConvert.DeserializeObject<MsgWrapper<Object>>(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        SendNextMove(line);
                        break;
                    case "join":
                        log("Joined");
                        send(new PingMessage());
                        break;
                    case "gameInit":
                        log("Race init");
                        MsgWrapper<GameInitDto> gameInitmsg = JsonConvert.DeserializeObject<MsgWrapper<GameInitDto>>(line);
                        Track track = new TrackAssembler().CreateTrack(gameInitmsg.data.Race.Track);
                        if (bot == null)
                            bot = botFactory.CreateBot(track);
                        else
                            bot.Track = track;
                        fullLog.AppendLine(gameInitmsg.data.ToString());
                        send(new PingMessage());
                        break;
                    case "gameEnd":
                        log("Race ended");
                        MsgWrapper<GameEndDto> gameEndMsg = JsonConvert.DeserializeObject<MsgWrapper<GameEndDto>>(line);
                        BestLapMillis = gameEndMsg.data.BestLaps[0].Result.Millis;
                        send(new PingMessage());
                        break;
                    case "gameStart":
                        log("Race starts");
                        send(new PingMessage());
                        break;
                    case "crash":
                        log("crash");
                        fullLog.AppendLine("crash");
                        send(new PingMessage());
                        //Thread.Sleep(5000);
                        return; // cancel run for testing purposes
                    case "turboAvailable":
                        log("turbo");
                        fullLog.AppendLine("turbo");
                        bot.ProvideTurbo();
                        break;
                    case "turboStart":
                        Console.WriteLine("turboStart");
                        break;
                    case "turboEnd":
                        Console.WriteLine("turboEnd");
                        break;
                    default:
                        send(new PingMessage());
                        break;
                }
            }
        }

        void SendNextMove(string line)
        {
            MsgWrapper<CarPositionDto[]> carPositionMsg = JsonConvert.DeserializeObject<MsgWrapper<CarPositionDto[]>>(line);
            List<CarPositionDto> positions = new List<CarPositionDto>(carPositionMsg.data);
            CarPositionAssembler assembler = new CarPositionAssembler();
            CarPositionDto positionDto = positions.Find(p => p.Id.Name.Equals(connParams.BotName));
            CarPosition position = assembler.CreateCarPosition(positionDto);
            fullLog.AppendLine(positionDto.ToString() + "\tThrottle\t" + previousThrottle);

            if (currentPieceIndex != position.PieceIndex)
            {
                currentPieceIndex = position.PieceIndex;
                log("Lap " + position.Lap + ", piece " + position.PieceIndex + ", lane " + position.Lane);
            }

            Command command = bot.GetCommand(assembler.CreateCarPositions(positions));
            switch (command.SwitchLane)
            {
                case SwitchLane.None:
                    if (command.Turbo)
                    {
                        Console.WriteLine("send turbo");
                        send(new TurboMessage());
                    }
                    else
                        send(new ThrottleMessage(command.Throttle));
                    break;
                case SwitchLane.Left:
                    send(new SwitchLaneLeftMessage());
                    break;
                case SwitchLane.Right:
                    send(new SwitchLaneRightMessage());
                    break;
                default:
                    throw new Exception("Unhandled SwitchLane value");
            }
            previousThrottle = command.Throttle;
        }

        void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }

        void log(String msg)
        {
            if (logEnabled)
                Console.WriteLine(msg);
        }
    }
}
