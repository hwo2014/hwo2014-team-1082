﻿using Lla.HwoBot.AI;
using Lla.HwoBot.Messages;
using Lla.HwoBot.Messages.Assemblers;
using Lla.HwoBot.Messages.Dtos;
using Lla.HwoBot.Race;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;

namespace Lla.HwoBot.Racing
{
    class RaceBotRunner
    {
        ConnectionParameters connectionParams;
        StreamReader reader;
        StreamWriter writer;
        string myColor;
        Track track;
        List<Car> cars;
        CarPosition myPosition;
		Physics physics;

        public void Run(ConnectionParameters connectionParams)
        {
            if (connectionParams == null)
                throw new ArgumentNullException("connectionParams");

            this.connectionParams = connectionParams;
            log("Connecting to " + connectionParams.Host + ":" + connectionParams.Port + " as " +
                connectionParams.BotName + "/" + connectionParams.BotKey);

            using (TcpClient client = new TcpClient(connectionParams.Host, connectionParams.Port))
            {
                NetworkStream stream = client.GetStream();
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                writer.AutoFlush = true;
                run();
            }
        }

        void run()
        {
			send(new JoinMessage(connectionParams.BotName, connectionParams.BotKey));
			//send(new JoinTrackMessage(connectionParams.BotName, connectionParams.BotKey, "france", 1));
            string line;
            RaceBot bot = null;
            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper<Object> msg = JsonConvert.DeserializeObject<MsgWrapper<Object>>(line);
				//   log(line);
                switch (msg.msgType)
                {
					case "crash":
						crash(bot, line);
                        break;
                    case "carPositions":
                        bot.UpdatePositions(getCarPositions(line));
                        sendCommand(bot.GetCommand());
                        break;
					case "gameInit":
                        initGame(line);
						bot = new RaceBot(myColor, cars, track, physics);
                        break;
					case "spawn":
						spawn(bot, line);
                        break;
					case "turboAvailable":
						log ("turbo available");
						bot.Turbo ();
						break;
                    case "yourCar":
                        getMyCarId(line);
                        break;
                    default:
                        log("Server message: " + msg.msgType);
                        send(new PingMessage());
                        break;
                }
            }
        }

        private void sendCommand(Command command)
        {
			if (command.SwitchLane == SwitchLane.Left)
				send (new SwitchLaneLeftMessage ());
			else if (command.SwitchLane == SwitchLane.Right)
				send (new SwitchLaneRightMessage ());
			else if (command.Turbo) {
				Console.WriteLine ("send: turbo");
				send (new TurboMessage ());
			}
            else
                send(new ThrottleMessage(command.Throttle));
        }

        void getMyCarId(string line)
        {
            CarIdDto dto = parseMsg<CarIdDto>(line);
            myColor = dto.Color;
            Console.WriteLine("My car: " + dto.Name + " (" + dto.Color + ")");
        }

        void initGame(string line)
        {
            GameInitDto dto = parseMsg<GameInitDto>(line);
			if (track == null) {
				track = new TrackAssembler ().CreateTrack (dto.Race.Track);
				physics = new Physics (track);
			}
            cars = new CarAssembler().CreateCars(dto.Race.Cars);
            log("gameInit");
        }

		void crash(RaceBot bot, string line) 
		{
			CarIdDto dto = parseMsg<CarIdDto> (line);
			if (dto.Color == myColor) {
				log("crash");
				bot.Crash();
			}
		}

		void spawn(RaceBot bot, string line) 
		{
			CarIdDto dto = parseMsg<CarIdDto> (line);
			if (dto.Color == myColor) {
				log("spawn");
				bot.Spawn();
			}
		}

        List<CarPosition> getCarPositions(string line)
        {
            CarPositionDto[] dtos = parseMsg<CarPositionDto[]>(line);
            List<CarPosition> positions = new CarPositionAssembler().CreateCarPositions(dtos);
            CarPosition myOldPosition = myPosition;
            myPosition = positions.Find(p => p.Color == myColor);
            if (myOldPosition == null || myOldPosition.PieceIndex != myPosition.PieceIndex)
            {
                log("Lap: " + myPosition.Lap + ", piece: " + myPosition.PieceIndex + ", lane: " +
                    myPosition.Lane);
            }
            return positions;
        }

        T parseMsg<T>(string line)
        {
            return JsonConvert.DeserializeObject<MsgWrapper<T>>(line).data;
        }

        void send(SendMsg msg)
        {
			//	log("Sent: " + msg.ToJson());
            writer.WriteLine(msg.ToJson());
        }

        void log(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
