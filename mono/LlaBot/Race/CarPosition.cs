﻿using System;

namespace Lla.HwoBot.Race
{
    class CarPosition
    {
        public CarPosition(string color, double angle, int lane, int goingToLane, int lap, int pieceIndex, double inPieceDistance)
        {
            if (color == null)
                throw new ArgumentNullException("name");
            if (lane < 0)
                throw new ArgumentException("lane must be at least 0", "lane");
            if (goingToLane < 0)
                throw new ArgumentException("goingToLane must be at least 0", "goingToLane");
			if (lap < -1)
				throw new ArgumentException("lap must be at least -1", "lap");
            if (pieceIndex < 0)
                throw new ArgumentException("pieceIndex must be at least 0", "pieceIndex");
            if (inPieceDistance < 0)
                throw new ArgumentException("inPieceDistance must be at least 0", "inPieceDistance");
            Color = color;
            Angle = angle;
            Lane = lane;
            GoingToLane = goingToLane;
            Lap = lap;
            PieceIndex = pieceIndex;
            InPieceDistance = inPieceDistance;
        }

        public double Angle { get; private set; }

        public string Color { get; private set; }

        public int Lane { get; private set; }

        public int GoingToLane { get; private set; }

        public int Lap { get; private set; }

        public int PieceIndex { get; private set; }

        public double InPieceDistance { get; private set; }

        public bool IsSame(CarPosition position)
        {
            if (position == null)
                return false;
            if (position.PieceIndex != PieceIndex)
                return false;
            return Math.Abs(position.InPieceDistance - InPieceDistance) < 1.0;
        }
    }
}
