﻿using System;
using System.Collections.Generic;

namespace Lla.HwoBot.Race
{
    class Track
    {
        const double epsilon = 0.000001;
        List<TrackPiece> pieces;
        List<double> curveStarts = new List<double>();
        Dictionary<int, double> lanes;


        public Track(TrackPiece[] pieces, Dictionary<int, double> lanes)
        {
            if (pieces == null)
                throw new ArgumentNullException("pieces");
            if (lanes == null)
                throw new ArgumentNullException("lanes");
            Pieces = new List<TrackPiece>(pieces);
            this.lanes = new Dictionary<int, double>(lanes);
			GetFirstPieceOfLongestStraight ();
        }

		public int LaneCount
		{
			get 
			{
				return lanes.Count;
			}
		}

        public List<TrackPiece> Pieces
        {
            get { return new List<TrackPiece>(pieces); }
            private set { pieces = value; }
        }

        public double GetLength()
        {
            double length = 0.0;
            foreach (TrackPiece piece in pieces)
                length += piece.Length;
            return length;
        }

        public double GetDistanceFromCenter(int lane)
        {
            return lanes[lane];
        }

        public double GetPieceStartPosition(int index)
        {
            if (index < 0 || index >= pieces.Count)
                throw new ArgumentException("Index out of range (" + index + ")", "index");
            double result = 0.0;
            for (int i = 0; i < index; i++)
                result += pieces[i].Length;
            return result;
        }

        public List<double> GetCurveStarts()
        {
            if (curveStarts.Count == 0)
            {
                for (int i = 1; i < Pieces.Count; i++)
                {
                    if (fromStraightToCurve(i) || oppositeCurve(i))
                        curveStarts.Add(GetPieceStartPosition(i));
                }
            }
            return curveStarts;
        }

		public int GetFirstPieceOfLongestStraight() 
		{
			double longestLength = 0.0;
			int longestStart = -1;
			double currentLength = 0.0;
			int currentStart = -1;
			for (int i = 0; i < 2 * pieces.Count; i++) {
				int index = i % pieces.Count;
				if (pieces [index].IsTurn) {
					if (currentLength > longestLength) {
						longestLength = currentLength;
						longestStart = currentStart;
					}
					currentLength = 0.0;
					currentStart = -1;
					if (i >= pieces.Count)
						break;
				} else {
					if (currentStart == -1)
						currentStart = index;
					currentLength += pieces [index].Length;
				}
				//Console.WriteLine(index + " " + pieces[index].Length + " " + pieces[index].IsTurn + " " + currentStart + " " + currentLength + " " + longestStart + " " + longestLength);
			}
			//Console.WriteLine ("GetFirstPieceOfLongestStraight: " + longestStart + " " + longestLength);
			return longestStart;
		}

		public TrackPiece GetNextTurnPieceIndex(int pieceIndex)
		{
			for (int i = pieceIndex + 1; i < 2 * pieces.Count; i++) {
				TrackPiece piece = pieces[i % pieces.Count];
				if (piece.IsTurn)
					return piece;
			}
			return null;
		}

        bool fromStraightToCurve(int pieceIndex)
        {
            double previousAngle = Math.Abs(Pieces[pieceIndex - 1].Angle);
            double currentAngle = Math.Abs(Pieces[pieceIndex].Angle);
            return previousAngle < epsilon && currentAngle > epsilon;
        }

        bool oppositeCurve(int pieceIndex)
        {
            double previousAngle = Pieces[pieceIndex - 1].Angle;
            double currentAngle = Pieces[pieceIndex].Angle;
            if (Math.Abs(previousAngle) < epsilon)
                return false;
            if (Math.Abs(currentAngle) < epsilon)
                return false;
            return previousAngle * currentAngle < -epsilon;
        }
    }
}
