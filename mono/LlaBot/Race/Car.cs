﻿namespace Lla.HwoBot.Race
{
    class Car
    {
        public Car(string name, double length, double width, double guideFlag)
        {
            Name = name;
            Length = length;
            Width = width;
            GuideFlag = guideFlag;
        }

        public string Name { get; private set; }

        public double Length { get; private set; }

        public double Width { get; private set; }

        public double GuideFlag { get; private set; }
    }
}
