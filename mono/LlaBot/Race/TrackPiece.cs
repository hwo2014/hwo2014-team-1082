﻿using System;

namespace Lla.HwoBot.Race
{
    abstract class TrackPiece
    {
        public TrackPiece(bool hasSwitch)
        {
            HasSwitch = hasSwitch;
        }

        public bool HasSwitch { get; private set; }

        public abstract double Length { get; }

        public abstract double Angle { get; }

        public abstract double Radius { get; }

        public abstract double GetLaneLength(int distanceFromCenter);

        public bool IsTurn
        {
            get
            {
                return Math.Abs(Angle) >= 0.000001;
            }
        }
    }
}
