﻿using System;
using System.Collections.Generic;

namespace Lla.HwoBot.Race
{
    class Physics
    {
        Track track;
        List<TrackPiece> pieces;
        double maxCentripedalForce = Double.PositiveInfinity;
        double drag = 0.0;
        double mass = 0.0;

        public Physics(Track track)
        {
            if (track == null)
                throw new ArgumentNullException("track");
            this.track = track;
            pieces = track.Pieces;
        }

        public void InitSpeed(CarPosition p0, CarPosition p1, CarPosition p2, CarPosition p3)
        {
            if (p0 == null)
                throw new ArgumentNullException("p0");
            if (p1 == null)
                throw new ArgumentNullException("p1");
            if (p2 == null)
                throw new ArgumentNullException("p2");
            if (p3 == null)
                throw new ArgumentNullException("p3");
            double v1 = GetSpeed(p0, p1);
            double v2 = GetSpeed(p1, p2);
            double v3 = GetSpeed(p2, p3);
            double throttle = 1.0;
            drag = (v1 - (v2 - v1)) / (v1 * v1) * throttle;
            mass = 1.0 / Math.Log((v3 - throttle / drag) / (v2 - throttle / drag)) * -drag;
        }

        public void Crash(CarPosition previous, CarPosition current)
        {
            if (previous == null)
                throw new ArgumentNullException("previous");
            if (current == null)
                throw new ArgumentNullException("current");

            TrackPiece piece = track.Pieces[current.PieceIndex];
			double r = getRadius (piece, current.Lane);
			double v = GetSpeed(previous, current);
			if (piece.IsTurn)
				maxCentripedalForce = Math.Min (0.8 * v * v / r, maxCentripedalForce * 0.95);
			else
				maxCentripedalForce = maxCentripedalForce * 0.95;
			Console.WriteLine ("maxCentripedalForce: " + maxCentripedalForce + " " + v + " " + r);
        }

        public double GetBreakingDistanceForNextTurn(CarPosition previous, CarPosition current)
        {
            if (current == null)
                throw new ArgumentNullException("position");
            int nextTurnIndex = getNextTurnIndex(current);
            if (nextTurnIndex == current.PieceIndex)
                return 0.0;

            double v0 = GetSpeed(previous, current);
            double v = GetCrashSpeed(pieces[nextTurnIndex], current.Lane);
            if (v == Double.PositiveInfinity)
                return 0.0;

			double breakingTicks = getTicksToSpeed(v0, v, 0.0);
			//Console.WriteLine ("GetBreakingDistanceForNextTurn: " + v0 + " " + v + " " + breakingTicks);
            return getDistanceAfterTicks(breakingTicks, v0, 0.0);
        }

        public double GetCrashSpeed(TrackPiece piece, int lane)
        {
            if (piece == null)
                throw new ArgumentNullException("piece");
            if (piece.IsTurn && maxCentripedalForce < Double.PositiveInfinity)
				return Math.Sqrt(maxCentripedalForce * getRadius(piece, lane));
            else
                return Double.PositiveInfinity;
        }

        public double GetDistanceToNextTurn(CarPosition position)
        {
            int pieceIndex = position.PieceIndex;
            double distance = -position.InPieceDistance;
            while (true)
            {
                TrackPiece piece = pieces[pieceIndex];
                if (piece.IsTurn)
                    return Math.Max(0.0, distance);
                distance += piece.Length;
                if (++pieceIndex >= pieces.Count)
                    pieceIndex = 0;
            }
        }

        public double GetTerminalSpeed(double throttle)
        {
            return throttle / drag;
        }

        public double GetThrottleForTerminalSpeed(double v)
        {
            return v * drag;
        }

        double getDistanceAfterTicks(double ticks, double v0, double throttle)
        {
			return mass / drag * (v0 - throttle / drag) * (1.0 - Math.Exp(-drag * ticks / mass)) +
                throttle / drag * ticks;
        }

		double getSpeedAfterTicks(double ticks, double v0, double throttle) 
		{
			return (v0 - throttle / drag) * Math.Exp (-drag * ticks / mass) + throttle / drag;
		}

        double getTicksToSpeed(double v0, double v, double throttle)
        {
            return Math.Log((v - throttle / drag) / (v0 - throttle / drag)) * mass / -drag;
        }

        int getNextTurnIndex(CarPosition position)
        {
            int index = pieces.FindIndex(position.PieceIndex, p => p.IsTurn);
            if (index < 0)
                index = pieces.FindIndex(p => p.IsTurn);
            return index;
        }

        public double GetSpeed(CarPosition previous, CarPosition current)
        {
            if (previous == null)
                throw new ArgumentNullException("previous");
            if (current == null)
                throw new ArgumentNullException("current");

            double v = current.InPieceDistance - previous.InPieceDistance;
			if (v < 0) {
				v += pieces [previous.PieceIndex].GetLaneLength (previous.Lane);
				if (pieces [previous.PieceIndex].HasSwitch)
					v += 3.0;
			}
            return v;
        }

		double getRadius(TrackPiece piece, int lane)
		{
			double distanceFromCenter = track.GetDistanceFromCenter(lane);
			return piece.Radius + (piece.Angle > 0.0 ? -1 : 1) * distanceFromCenter;
		}
    }
}
