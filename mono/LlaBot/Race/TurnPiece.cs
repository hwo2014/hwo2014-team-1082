﻿using System;

namespace Lla.HwoBot.Race
{
    class TurnPiece : TrackPiece
    {
        double angle;
        double radius;

        public TurnPiece(double radius, double angle)
            : base(/* hasSwitch = */ false)
        {
            if (radius <= 0.0)
                throw new ArgumentException("radius must be positive", "radius");
            this.radius = radius;
            this.angle = angle;
        }

        public override double Radius
        {
            get
            {
                return radius;
            }
        }

        public override double Angle
        {
            get { return angle; }
        }

        public override double Length
        {
            get { return Math.Abs(Angle) / 360.0 * 2 * Math.PI * Radius; }
        }

        public override double GetLaneLength(int distanceFromCenter)
        {
            return Length * (Radius + distanceFromCenter) / Radius;
        }
    }
}
