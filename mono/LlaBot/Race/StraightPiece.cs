﻿using System;

namespace Lla.HwoBot.Race
{
    class StraightPiece : TrackPiece
    {
        private double length;

        public StraightPiece(double length, bool hasSwitch)
            : base(hasSwitch)
        {
            if (length <= 0.0)
                throw new ArgumentException("length must be positive", "length");
            this.length = length;
        }

        public override double Angle
        {
            get { return 0.0; }
        }

        public override double Length {
            get { return length; }
        }

        public override double GetLaneLength(int distanceFromCenter)
        {
            return Length;
        }

        public override double Radius
        {
            get { return Double.PositiveInfinity; }
        }
    }
}
