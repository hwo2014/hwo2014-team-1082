﻿using Lla.HwoBot.AI;
using Lla.HwoBot.Racing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

public class Program {

    static StringBuilder log = new StringBuilder();

	public static void Main(string[] args) {
        try
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];
            string trackName = args.Length > 4 ? (args[4] == "finland" ? "" : args[4]) : "";
            int populationSize = args.Length > 5 ? Int32.Parse(args[5]) : 1;
            List<string> argsList = new List<string>(args);

            ConnectionParameters p = new ConnectionParameters(getRandomServers(), host, port, botName, botKey);
            new RaceBotRunner().Run(p);
/*
            if (argsList.Contains("genetic"))
            {
                GeneticBotRunner botRunner = new GeneticBotRunner(p, populationSize);
                botRunner.Run(new GeneticSoloTrackSpecificBotFactory(trackName), trackName);
            }
            else if (populationSize == 1)
            {
                BotRunner botRunner = new BotRunner(p);
                //botRunner.Run(0, new TestBotFactory(), trackName);
                //botRunner.Run(0, new ConstantSpeedBotFactory(0.65), trackName);
                botRunner.Run(0, new SoloTrackSpecificBotFactory(trackName), trackName);
                //trackName = "germany";
            }
            else
                new ParallelBotRunner(p, 10).Run(new SoloTrackSpecificBotFactory(trackName), trackName);
 */
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            Thread.Sleep(10000);
        }
 	 }

    static string[] getRandomServers()
    {
        return new string[] { "testserver", "hakkinen", "senna", "webber" };
    }
}
