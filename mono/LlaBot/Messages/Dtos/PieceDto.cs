﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class PieceDto : Dto
    {
        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("switch")]
        public bool Switch { get; set; }

        [JsonProperty("radius")]
        public double Radius { get; set; }

        [JsonProperty("angle")]
        public double Angle { get; set; }
    }
}
