﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class CarResultDto
    {
        [JsonProperty("car")]
        public CarIdDto Car { get; set; }

        [JsonProperty("result")]
        public ResultDto Result { get; set; }
    }
}
