﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class CarPositionDto : Dto
    {
        [JsonProperty("id")]
        public CarIdDto Id { get; set; }

        [JsonProperty("angle")]
        public double Angle { get; set; }

        [JsonProperty("piecePosition")]
        public PiecePositionDto PiecePosition { get; set; }
    }
}
