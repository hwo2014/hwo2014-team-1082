﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class StartingPointDto : Dto
    {
        [JsonProperty("position")]
        public PositionDto Position { get; set; }

        [JsonProperty("angle")]
        public double Angle { get; set; }
    }
}
