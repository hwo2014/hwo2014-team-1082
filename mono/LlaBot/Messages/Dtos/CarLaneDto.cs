﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class CarLaneDto : Dto
    {
        [JsonProperty("startLaneIndex")]
        public int StartLaneIndex { get; set; }

        [JsonProperty("endLaneIndex")]
        public int EndLaneIndex { get; set; }
    }
}
