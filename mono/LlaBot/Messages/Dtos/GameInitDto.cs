﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class GameInitDto : Dto
    {
        [JsonProperty("race")]
        public RaceDto Race { get; set; }
    }
}
