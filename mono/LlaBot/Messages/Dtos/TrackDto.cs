﻿using Newtonsoft.Json;
using System.Text;

namespace Lla.HwoBot.Messages.Dtos
{
    class TrackDto : Dto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pieces")]
        public PieceDto[] Pieces { get; set; }

        [JsonProperty("lanes")]
        public TrackLaneDto[] Lanes { get; set; }

        [JsonProperty("startingPoint")]
        public StartingPointDto StartingPoint { get; set; }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("Id\t" + Id + "\tName\t" + Name + "\tPieces\n");
            for (int i = 0; i < Pieces.Length; i++)
                result.AppendLine(i + "\t" + Pieces[i].ToString());
            foreach(TrackLaneDto lane in Lanes)
                result.AppendLine(lane.ToString());
            result.Append("\tStartingPoint\t" + StartingPoint);
            return result.ToString();
        }
    }
}
