﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class GameEndDto
    {
        [JsonProperty("results")]
        public CarResultDto[] Results { get; set; }

        [JsonProperty("bestLaps")]
        public CarResultDto[] BestLaps { get; set; }
    }
}
