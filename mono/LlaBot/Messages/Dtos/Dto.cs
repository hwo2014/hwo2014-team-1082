﻿using System.Reflection;
using System.Text;

namespace Lla.HwoBot.Messages.Dtos
{
    class Dto
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (PropertyInfo property in GetType().GetProperties())
                result.Append(property.Name + "\t" + property.GetValue(this, null) + "\t");
            return result.ToString();
        }
    }
}
