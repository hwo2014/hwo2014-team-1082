﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class CarDimensionsDto : Dto
    {
        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("width")]
        public double Width { get; set; }

        [JsonProperty("guideFlagPosition")]
        public double GuideFlagPosition { get; set; }
    }
}
