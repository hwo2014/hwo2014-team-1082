﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class RaceDto : Dto
    {
        [JsonProperty("track")]
        public TrackDto Track { get; set; }

        [JsonProperty("cars")]
        public CarDto[] Cars { get; set; }

        [JsonProperty("raceSession")]
        public RaceSessionDto Session { get; set; }
    }
}
