﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class CarIdDto : Dto
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }
}
