﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class RaceSessionDto : Dto
    {
        [JsonProperty("laps")]
        public int Laps { get; set; }

        [JsonProperty("maxLapTimeMs")]
        public int MaxLapTimeMs { get; set; }

        [JsonProperty("quickRace")]
        public bool QuickRace { get; set; }
    }
}
