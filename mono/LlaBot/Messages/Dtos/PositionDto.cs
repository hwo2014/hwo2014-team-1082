﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class PositionDto : Dto
    {
        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }
    }
}
