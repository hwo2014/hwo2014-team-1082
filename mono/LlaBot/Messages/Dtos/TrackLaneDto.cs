﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class TrackLaneDto : Dto
    {
        [JsonProperty("distanceFromCenter")]
        public int DistanceFromCenter { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }
    }
}
