﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class ResultDto
    {
        [JsonProperty("laps")]
        public int Laps { get; set; }

        [JsonProperty("ticks")]
        public int Ticks { get; set; }

        [JsonProperty("millis")]
        public int Millis { get; set; }
    }
}
