﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class PiecePositionDto : Dto
    {
        [JsonProperty("pieceIndex")]
        public int PieceIndex { get; set; }

        [JsonProperty("inPieceDistance")]
        public double InPieceDistance { get; set; }

        [JsonProperty("lane")]
        public CarLaneDto Lane { get; set; }

        [JsonProperty("lap")]
        public int Lap { get; set; }
    }
}
