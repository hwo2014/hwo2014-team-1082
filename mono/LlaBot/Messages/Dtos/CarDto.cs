﻿using Newtonsoft.Json;

namespace Lla.HwoBot.Messages.Dtos
{
    class CarDto : Dto
    {
        [JsonProperty("id")]
        public CarIdDto Id { get; set; }

        [JsonProperty("dimensions")]
        public CarDimensionsDto Dimensions { get; set; }
    }
}
