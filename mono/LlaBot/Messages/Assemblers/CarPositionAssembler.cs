﻿using Lla.HwoBot.Messages.Dtos;
using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.Messages.Assemblers
{
    class CarPositionAssembler
    {
        public CarPositionAssembler() { }

        public List<CarPosition> CreateCarPositions(IList<CarPositionDto> dtos)
        {
            if (dtos == null)
                throw new ArgumentNullException("dtos");
            List<CarPosition> result = new List<CarPosition>(dtos.Count);
            foreach (CarPositionDto dto in dtos)
                result.Add(CreateCarPosition(dto));
            return result;
        }

        public CarPosition CreateCarPosition(CarPositionDto dto)
        {
            if (dto == null)
                throw new ArgumentNullException("dto");
            return new CarPosition(dto.Id.Color, dto.Angle, dto.PiecePosition.Lane.StartLaneIndex, 
                dto.PiecePosition.Lane.EndLaneIndex, dto.PiecePosition.Lap, 
                dto.PiecePosition.PieceIndex, dto.PiecePosition.InPieceDistance);
        }
    }
}
