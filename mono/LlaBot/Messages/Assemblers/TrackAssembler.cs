﻿using Lla.HwoBot.Messages.Dtos;
using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.Messages.Assemblers
{
    class TrackAssembler
    {
        public TrackAssembler() { }

        public Track CreateTrack(TrackDto dto)
        {
            if (dto == null)
                throw new ArgumentNullException("dto");
            Track track = new Track(createPieces(dto.Pieces), createLanes(dto.Lanes));
            return track;
        }

        TrackPiece[] createPieces(PieceDto[] dtos)
        {
            TrackPiece[] pieces = new TrackPiece[dtos.Length];
            for (int i = 0; i < dtos.Length; i++)
                pieces[i] = createPiece(dtos[i]);
            return pieces;
        }

        TrackPiece createPiece(PieceDto dto)
        {
            if (dto.Length > 0.000001)
                return new StraightPiece(dto.Length, dto.Switch);
            else
                return new TurnPiece(dto.Radius, dto.Angle);
        }

        Dictionary<int, double> createLanes(TrackLaneDto[] dtos)
        {
            Dictionary<int, double> result = new Dictionary<int, double>();
            foreach (TrackLaneDto dto in dtos)
                result.Add(dto.Index, dto.DistanceFromCenter);
            return result;
        }
    }
}
