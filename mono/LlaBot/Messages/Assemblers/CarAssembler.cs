﻿using Lla.HwoBot.Messages.Dtos;
using Lla.HwoBot.Race;
using System;
using System.Collections.Generic;

namespace Lla.HwoBot.Messages.Assemblers
{
    class CarAssembler
    {
        public List<Car> CreateCars(CarDto[] dtos)
        {
            if (dtos == null)
                throw new ArgumentNullException("dtos");

            List<Car> cars = new List<Car>(dtos.Length);
            foreach (CarDto dto in dtos)
            {
                cars.Add(new Car(dto.Id.Name, dto.Dimensions.Length, dto.Dimensions.Width,
                    dto.Dimensions.GuideFlagPosition));
            }
            return cars;
        }
    }
}
