﻿using System;

namespace Lla.HwoBot.Messages
{
    class JoinTrackMessage : SendMsg
    {
        public BotId botId;
        public string trackName;
        public int carCount;

        public JoinTrackMessage(string name, string key, string trackName, int carCount)
        {
            if (String.IsNullOrEmpty(trackName))
                throw new ArgumentNullException("trackname");
            if (carCount <= 0)
                throw new ArgumentException("carCount must be positive", "carCount");
            botId = new BotId(name, key);
            this.trackName = trackName;
            this.carCount = carCount;
        }

        public class BotId
        {
            public string name;
            public string key;

            public BotId(string name, string key)
            {
                if (String.IsNullOrEmpty(name))
                    throw new ArgumentNullException("name");
                if (String.IsNullOrEmpty(key))
                    throw new ArgumentNullException("key");
                this.name = name;
                this.key = key;
            }
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }
}
