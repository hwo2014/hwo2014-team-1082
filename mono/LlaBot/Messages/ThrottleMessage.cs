﻿using System;

namespace Lla.HwoBot.Messages
{
    class ThrottleMessage : SendMsg
    {
        public double value;

        public ThrottleMessage(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}
