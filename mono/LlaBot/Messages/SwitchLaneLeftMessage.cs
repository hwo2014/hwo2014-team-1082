﻿using System;

namespace Lla.HwoBot.Messages
{
    class SwitchLaneLeftMessage : SwitchLaneMessage
    {
        protected override Object MsgData()
        {
            return "Left";
        }
    }
}
