﻿using System;

namespace Lla.HwoBot.Messages
{
    class SwitchLaneRightMessage : SwitchLaneMessage
    {
        protected override Object MsgData()
        {
            return "Right";
        }
    }
}
