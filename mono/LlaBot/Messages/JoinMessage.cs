﻿
namespace Lla.HwoBot.Messages
{
    class JoinMessage : SendMsg
    {
        public string name;
        public string key;

        public JoinMessage(string name, string key)
        {
            this.name = name;
            this.key = key;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

}
