﻿using Newtonsoft.Json;
using System;

namespace Lla.HwoBot.Messages
{
    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper<Object>(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}
