﻿namespace Lla.HwoBot.Messages
{
    class TurboMessage : SendMsg
    {
        protected override string MsgType()
        {
            return "turbo";
        }

        protected override object MsgData()
        {
            return "boost";
        }
    }
}
